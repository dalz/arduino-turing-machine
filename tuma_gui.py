#!/usr/bin/env python

# Arduino Turing Machine
# Copyright (C) 2019  dalz

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import tuma
import os
import time

import threading
from threading import Thread

from tkinter import *
from tkinter.scrolledtext import ScrolledText

tm = tuma.TuringMachine()
ins_modified = False
tape_modified = False

t = Thread()

prog_dir = "progs"
progs = []

def display_expn(_=0):
    l = ins_box.index(INSERT)

    expn_text = ""
    line = ins_box.get(f"{l} linestart", f"{l} lineend")
    print("esc:", tuma.escape(line))
    print("exp:", tuma.ins_expand(tuma.escape(line)))
    for ins in map(tuma.unescape, tuma.ins_expand(tuma.escape(line))):
        expn_text += ins + '\n'

    expn_box.config(state=NORMAL)
    expn_box.delete("1.0", END)
    expn_box.insert("1.0", expn_text)
    expn_box.config(state=DISABLED)

def set_ins_modified(_=0):
    global ins_modified
    ins_modified = True

def set_tape_modified(_):
    global tape_modified
    tape_modified = True

def set_delay_modified(_):
    global delay_modified
    delay_modified = True

def send_tape(_=0):
    global tape_modified
    tm.set_tape(tape_box.get())
    tape_modified = False

def tm_step():
    global ins_modified, tape_modified
    if ins_modified:
        tm.set_instructions(ins_box.get("1.0", END))
        ins_modified = False
    if tape_modified:
        send_tape()

    line = tm.step()

    ins_box.tag_remove("exec", "1.0", END)
    if line is not None:
        ins_box.tag_add("exec", f"{line}.0", f"{line}.end")

    print(tm.get_tape())
     
    return line

def tm_go_or_pause():
    global t
   
    if t.is_alive():
        t.event.set()
        return

    sec_s = delay_box.get()
    sec = int(sec_s) / 10 if sec_s else 0

    def run():
        while (tm_step() is not None):
            if t.event.is_set():
                return
            time.sleep(sec)

    def check():
        if not t.is_alive():
            go_pause_text.set("Go")
        else:
            root.after(100, check)

    t = threading.Thread(target=run, daemon=True)
    t.event = threading.Event()
    t.start()

    go_pause_text.set("Pause")
    check()

def load_file():
    if t.is_alive():
        tm_go_or_pause()
        
    with open(prog_dir + '/' + progs[listbox.curselection()[0]], 'r') as f:
        ins = f.read()
        tm.set_instructions(ins)

        ins_box.delete("1.0", END)
        tape_box.delete(0, END)

        ins_box.insert("1.0", ins)

        if ins[0:3] == "## ":
            tape_box.insert(0, ins.split('\n')[0][3:])
            Thread(target=send_tape, daemon=True).start()

def move_tape(d):
    global tape_modified
    tape_modified = TRUE

    tm._move(d)
    tm._ser_send(d)

def reset():
    tm.reset()
    send_tape()

root = Tk()
root.title("Turing Machine")

ins_box = ScrolledText(root)
ins_box.pack(side=LEFT, fill=Y)
ins_box.bind("<Key>", set_ins_modified)
ins_box.bind("<Key>", display_expn, True)
ins_box.bind("<Button-1>", display_expn)
ins_box.tag_configure("exec", background="yellow")

tape_box = Entry(root, background="yellow")
tape_box.pack(fill=X)
tape_box.bind("<Key>", set_tape_modified)
tape_box.bind("<Return>", send_tape)

delay_box = Entry(root)
delay_box.pack()
delay_box.bind("<Key>", set_delay_modified)
delay_box.insert(0, "5")

go_pause_text = StringVar(value="Go")
go_pause_button = Button(root, textvariable=go_pause_text, command=tm_go_or_pause)
go_pause_button.pack(fill=X)

reset_button = Button(root, text="Reset", command=reset)
reset_button.pack(fill=X)

run_button = Button(root, text="Step", command=tm_step)
run_button.pack(fill=X)

move_tape_frame = Frame(root)
mt_left_button = Button(move_tape_frame, text="<", command=lambda: move_tape("<"))
mt_right_button = Button(move_tape_frame, text=">", command=lambda: move_tape(">"))
mt_left_button.pack(side=LEFT, expand=True, fill=X)
mt_right_button.pack(side=RIGHT, expand=True, fill=X)
move_tape_frame.pack(fill=X)

load_button = Button(root, text="Load", command=load_file)
load_button.pack(fill=X)

load_expn_frame = Frame(root)

listbox = Listbox(load_expn_frame)
listbox.pack(side=LEFT, expand=True, fill=BOTH)

progs = os.listdir(prog_dir)

for item in progs:
    listbox.insert(END, item)

expn_box = ScrolledText(load_expn_frame, state=DISABLED)
expn_box.pack(side=RIGHT, expand=True, fill=BOTH)

load_expn_frame.pack(expand=True, fill=BOTH)

if __name__ == "__main__":
    root.mainloop()




