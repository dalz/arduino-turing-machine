/*  Arduino Turing Machine
 *  Copyright (C) 2019  dalz
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <Wire.h>
#include <LiquidCrystal_I2C.h>

#include <MD_MAX72xx.h>
#include <SPI.h>

/* Arduino - LED matrix
 * 10 - CS
 * 11 - DIN
 * 13 - CLK
 *
 * Arduino - LCD
 * A4 - SDA
 * A5 - SCL
 */


#define MATRIX_NUM 12
#define CHAR_BUF_SIZE 5

MD_MAX72XX mx = MD_MAX72XX(MD_MAX72XX::FC16_HW, 10, MATRIX_NUM);

LiquidCrystal_I2C lcd(0x27, 16, 2);

typedef enum {
  STATE,
  ACTION,
} ReadProgress;

ReadProgress reading = STATE;
String buf = "";

char sym;

bool add_space;
int char_col = (CHAR_BUF_SIZE + CHAR_BUF_SIZE % 2) / 2;
uint8_t char_buf[CHAR_BUF_SIZE];

void set_state(String state) {
  lcd.setCursor(0, 1);
  lcd.print("                ");
  lcd.setCursor(0, 1);
  lcd.print(state);
}

// loads a char to char_buf
void load_char(char c) {
  int char_len = mx.getChar(c, CHAR_BUF_SIZE, char_buf);

  // add padding to the right until it's 5 columns long
  if (char_len < CHAR_BUF_SIZE) {
    for (int i = char_len; i < CHAR_BUF_SIZE; i++) {
      char_buf[i] = 0;
    }
  }
}

void tm_write(char c) {
  load_char(c);

  for (int i = 0; i < CHAR_BUF_SIZE; i++)
    mx.setColumn(MATRIX_NUM * 8 / 2 + 1 - i, char_buf[i]);
}

uint8_t tm_shift(uint8_t dev, MD_MAX72XX::transformType_t t) {
  load_char(sym);

  if ((t == MD_MAX72XX::TSR && char_col == CHAR_BUF_SIZE) ||
      (t == MD_MAX72XX::TSL && char_col == 0)) {
    // needed if shifting left
    // no effect on TSR, since this gets evaluated during the last call
    char_col++;

    // leave a blank column left of the character
    return 0;
  }

  // if shifting right, columns should be written last to first
  return char_buf[t == MD_MAX72XX::TSL ? char_col++ - 1 : CHAR_BUF_SIZE - ++char_col];
}

void setup() {
  Serial.begin(115200);

  mx.begin();
  mx.setShiftDataInCallback(tm_shift);

  lcd.init();
  lcd.backlight();
  lcd.print("stato:");
}

void loop() {
  if (Serial.available() > 0) {
    char c = Serial.read();

    if (c == '\n') {
      switch (reading) {
        case STATE:
          set_state(buf);
          reading = ACTION;
          Serial.print("state: ");
          Serial.println(buf);
          break;

        case ACTION:
          if (buf[0] == '-') {
            tm_write(buf[1]);

          } else {
            Serial.print("writing ");
            Serial.println(buf[1]);

            sym = buf[1];

            for (int n = 0; n <= CHAR_BUF_SIZE; n++) {
              if (n == (CHAR_BUF_SIZE + CHAR_BUF_SIZE % 2) / 2) {
                char_col = 0;
                sym = buf[2];
              }

              mx.transform(buf[0] == '>' ? MD_MAX72XX::TSR : MD_MAX72XX::TSL);
            }
          }

          reading = STATE;
          break;
      }

      buf = "";

    } else {
      buf += c;
    }
  }
}
