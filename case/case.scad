miw = 390;
mid = 32;
mh = 20;

smw = 32.5;
smh = 13;

mbw = 10;
mbd = 5;

mtw = miw + mbw * 2;
mtd = mid + mbd * 2;

sw = 5;
//sd = mtd + mbd / 2;
sh = 2;
sn = 8;

a = -15;

dpd = 38;
dpw = 82;

did = 26;
diw = 73;

dbw = 5;
dbd = 5;

dtw = dpw + dbw * 2;
dtd = dpd + dbd * 2;

dh = 18;

*rotate([a, 0, 0]) {
    difference() {
        cube([mtw, mtd, mh], true);
        cube([miw, mid, mh + 1], true);
    }

    for (x = [-miw / 2 + smw : smw : miw / 2 - smw]) {
        if (!(abs(x / smw) == 2))
            translate([x, 0, -(smh - mh /2)])
                cube([sw, mid + 1, sh], true);
    }
}

module foot() {
    hull() {
        rotate([a, 0, 0])
            translate([(mbw + miw) / 2, 0, -mh / 2 * cos(a)])
                cube([mbw, mtd, 1], true);

        translate([(mbw + miw) / 2, mh / 2 * sin(a), -(mh * cos(a) + mtd * abs(sin(a))) / 2])
            cube([mbw, mtd * cos(a), 0.1], true);
    }
}

*foot();
*mirror([1, 0, 0]) foot();

difference() {
    cube([dtw, dtd + dh * abs(tan(a)), dh + 2]);

    rotate([a, 0, 0])
        translate([-1, -dtd, 0])
            cube([dtw + 2, dtd, dh * 2]);

    translate([(dtw - diw) / 2, dh * abs(tan(a)) + (dtd - did) / 2, -1])
        cube([diw, did, dh + 4]);

    translate([(dtw - dpw) / 2, dh * abs(tan(a)) + (dtd - dpd) / 2, -1])
        cube([dpw, dpd, dh - 1]);
}
