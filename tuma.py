#!/usr/bin/env python3

# Arduino Turing Machine
# Copyright (C) 2019  dalz

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import serial
from serial.tools import list_ports

from time import sleep

CELLS_N = 17

OPEN_DELS="([{é"
CLOSED_DELS=")]}è"

cache = {}

def ins_expand(string):
    def char_range(s, e):
        i, j = ord(s), ord(e)

        if i < j:
            return ''.join([chr(c) for c in range(i, j + 1)])
        else:
            return ''.join([chr(c) for c in range(i, j - 1, -1)])

    def class_expand(string):
        res = ""
        oi = 0
        if not string:
            return ""

        while True:
            i = string.find("..", oi)

            if i == -1:
                res += string[oi:]
                return res

            res += string[oi:i-1]
            res += char_range(string[i - 1], string[i + 2])
            oi = i + 3

    def expand(string, od, cd):
        oi = 0
        esc = False

        temp = []
        cls = []

        for i, c in enumerate(string):
            if esc:
                esc = False
            elif c == '\\':
                esc = True
            elif c == od:
                temp.append(string[oi:i])
                oi = i + 1
            elif c == cd:
                cls.append(class_expand(string[oi:i]))
                oi = i + 1

        if oi != len(string):
            if not cls:
                return [string]

            temp.append(string[oi:])
            cls.append(None)

        res = []

        if cls:
            for i in range(0, len(cls[0])):
                s = ""
                for t, c in zip(temp, cls):
                    s += t + (c[i] if c is not None else '')
                res.append(s)

        return res

    res = [string]

    for o, c in zip(OPEN_DELS, CLOSED_DELS):
        old_res = res
        res = []

        for s in old_res:
            res.extend(expand(s, o, c))

    return res

def escape(string):
    string = string.replace('\\\\', '＼')
    string = string.replace('\\,', '⸴')
    string = string.replace('\\-', '—')

    for i, (o, c) in enumerate(zip(OPEN_DELS, CLOSED_DELS)):
        string = string.replace('\\' + o, chr(ord('α') + i))
        string = string.replace('\\' + c, chr(ord('Α') + i))

    return string

def unescape(string):
    string = string.replace('＼', '\\')
    string = string.replace('⸴', ',')
    string = string.replace('—', '\\-')

    for i, (o, c) in enumerate(zip(OPEN_DELS, CLOSED_DELS)):
        string = string.replace(chr(ord('α') + i), o)
        string = string.replace(chr(ord('Α') + i), c)

    return string

def ins_parse(string):
    ret = {}
    for (i, ln) in [(i + 1, l) for (i, l) in enumerate(string.split('\n')) if len(l.strip()) > 0]:
        if ln.strip()[0] == '#':
            continue

        for s in ins_expand(escape(ln)):
            try:
                state, read, new_state, write, move = map(unescape, map(str.strip, s.split(',')))
            except Exception as e:
                print(f"Error parsing string '{s}' at line {i}")
                raise e

            if read == '\\-':
                read = '-'
            elif read == '-':
                read = ' '
            if write == '\\-':
                write = '-'
            elif write == '-':
                write = ' '

            if not move in ("<", ">", "-"):
                raise ValueError(f"Invalid movement: '{move}' (line {i})")
            elif len(read) > 1:
                raise ValueError(f"Read symbol is not a single character: '{read}' (line {i})")
            elif len(write) > 1:
                raise ValueError(f"Written symbol is not a single character: '{write}'  (line {i})")

            ret[(state, read)] = (i, (new_state, write, move))

    return ret

class TuringMachine:
    def __init__(self):
        self.state = '0'
        self._right = True
        self._index = 0
        self._ins = {}
        self._tape = ([], [])
        self._start_tape = self._tape

        ports = [p.device for p in list_ports.comports()]
        self._ser = serial.Serial(ports[0] if ports else None, 115200)

    def __close__(self):
        self._ser.__close__()

    def set_port(port):
        self._ser.port = port

    def _ser_send(self, move, write=None):
        if self._ser.port is None:
            print("WARNING: no serial port connected")
            return
        
        if move == '-':
            action = move + self._read()

        else:
            action = '>' if move == '<' else '<'

            if move == '<':
                action += self.get_tape(CELLS_N)[:2][::-1]
            else:
                print("slice: '", self.get_tape(CELLS_N), "'")
                action += self.get_tape(CELLS_N)[-2:]

        if write:
            action = action[0] + write

        print("action: ", action)
        print("tape:", list(self.get_tape(CELLS_N)))
        print()
        self._ser.write(self.state.encode("ascii") +
                        b'\n' +
                        action.encode("ascii") +
                        b'\n')

    def _ser_send_tape(self):
        for _ in range(CELLS_N // 2):
            self._ser_send('>', "  ")

        for i in range(CELLS_N // 2 + 1, -1, -1):
            sleep(0.2) # the matrix display doesn't work correctly otherwise
            print("i: ", i)
            print("t: ", self._tape[1] + ((CELLS_N // 2) + 2) * [' '])
            self._ser_send('-', (self._tape[1] + ((CELLS_N // 2) + 2) * [' '])[i])

            # move the tape unless the last symbol has been written
            if i:
                self._ser_send('<', "  ")

    def reset(self):
        self.state = '0'
        self._right = True
        self._index = 0
        self._tape = self._start_tape

    def set_instructions(self, ins_str):
        if not ins_str in cache:
            cache[ins_str] = ins_parse(ins_str)
        self._ins = cache[ins_str]
        self.reset()

    def set_tape(self, tape):
        self.reset()
        self._tape = ([], list(tape))
        self._start_tape = self._tape
        self._ser_send_tape()

    def get_tape(self, slice_size=None):
        tape = self._tape[0][::-1] + self._tape[1]

        if slice_size is None:
            return ''.join(tape)

        else:
            half_slice = slice_size // 2
            tape = half_slice * [' '] + tape + half_slice * [' ']

            if self._right:
                i = self._index + len(self._tape[0]) + half_slice
            else:
                i = len(self._tape[0]) - self._index + half_slice

            return ''.join(tape[max(0, i - half_slice):i + (slice_size + 1) // 2])

    def _move(self, d):
        if not d == '-':
            self._index += (1 if d == '>' else -1) * (1 if self._right else -1)
            if self._index < 0:
                self._index = 0
                self._right = not self._right

    def _write(self, c):
        self._tape[self._right][self._index] = c

    def _read(self):
        try:
            return self._tape[self._right][self._index]
        except IndexError:
            self._tape[self._right].append(' ')
            return self._read()

    def step(self):
        try:
            ln, (new_state, write, move) = self._ins[(self.state, self._read())]
            self.state = new_state
            self._write(write)

            # first overwrite the current cell
            self._ser_send('-')

            # then shift the tape, both internally and on the matrix display
            self._move(move)
            self._ser_send(move)

            return ln

        except KeyError:
            return None
